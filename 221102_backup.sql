INSERT INTO inventory (item, category, amount) VALUES ('Bohnen', 'snäcks', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Boulgour', 'snäcks', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Kokosnussmilch', 'snäcks', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Kondensmilch', 'snäcks', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Pasta', 'snäcks', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Pasta', 'snäcks', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Polenta', 'snäcks', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Reis/ Vollreis', 'snäcks', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Risotto', 'snäcks', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Sämf', 'snäcks', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Schoggicreme (Pulver)', 'snäcks', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Sonnenblumenkerne', 'snäcks', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Tomatenpüree', 'snäcks', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Tomatensauce/ Pelati', 'snäcks', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Vanillecreme (Pulver)', 'snäcks', 3);

INSERT INTO inventory (item, category, amount) VALUES ('Alkoholfreies Bier', 'getränk', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Bier', 'getränk', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Bursinel 1 Fl', 'getränk', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Kaffeepulver', 'getränk', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Kaffeepulver', 'getränk', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Nescafe (Pulverkaffe)', 'getränk', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Prosecco', 'getränk', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Rosé', 'getränk', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Rotwein', 'getränk', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Weisswein', 'getränk', 3);

INSERT INTO inventory (item, category, amount) VALUES ('Alufolie', 'huushalt', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Anzündwürfel', 'huushalt', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Fonduepaste', 'huushalt', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Gaspatrone C206 GLS', 'huushalt', 1);
INSERT INTO inventory (item, category, amount) VALUES ('Grill-Aluschalen', 'huushalt', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Haushaltspapier (Rolle)', 'huushalt', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Kaffeefilter', 'huushalt', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Kehrichtsäck', 'huushalt', 1);
INSERT INTO inventory (item, category, amount) VALUES ('Mäuseköder', 'huushalt', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Schwamm zum Abwaschen', 'huushalt', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Spülmittel', 'huushalt', 3);
INSERT INTO inventory (item, category, amount) VALUES ('WC-Papier', 'huushalt', 3);

INSERT INTO inventory (item, category, amount) VALUES ('Aceto Balsamico', 'gwürz', 2);
INSERT INTO inventory (item, category, amount) VALUES ('Chrüütersalz', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Essig', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Gemüsebouillon', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Hühnerbouillon', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Muskatnuss', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Olivenöl', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Paprika', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Pfeffer', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Raclette-Gewürz', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Salz', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Sojasoosä', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Sonnenblumenöl/Bratöl', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Zimt', 'gwürz', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Zucker', 'gwürz', 3);

INSERT INTO inventory (item, category, amount) VALUES ('Alkali-Batterien für Radio (Grösse: LR14, C, Baby Zelle) 4 stk. fehlen', 'sonstiges', 1);
INSERT INTO inventory (item, category, amount) VALUES ('Kettensägeöl (am liebsten ökoplan)', 'sonstiges', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Moscht für Kettensäge (Aspen)', 'sonstiges', 3);
INSERT INTO inventory (item, category, amount) VALUES ('Seife', 'sonstiges', 3);






