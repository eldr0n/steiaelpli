FROM node:bullseye
EXPOSE 3002

WORKDIR /home/app

COPY package.json /home/app/
COPY package-lock.json /home/app/

RUN npm ci

COPY . /home/app

# RUN npm run build
CMD npm run start