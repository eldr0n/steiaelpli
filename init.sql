CREATE TABLE inventory (
  id            SERIAL PRIMARY KEY,
  item          VARCHAR(100)  NOT NULL,
  category      VARCHAR(100)  NOT NULL,
  amount        INT           NOT NULL
);

CREATE TABLE notes (
    id      SERIAL PRIMARY KEY,
    text    TEXT
);

INSERT INTO inventory (item, category, amount) VALUES ('spaghülnis', 'snäcks', 3);
INSERT INTO notes (text) VALUES ('hihi viel spass bim brucha, grüassli rico ^^');

