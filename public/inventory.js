const urlItems = "/items/";
const urlNotes = "/notes";
document.getElementById("add").disabled = true;
document.getElementById("add").addEventListener("click", function (event) {
    event.preventDefault();
});

function createItem(id, item, amount, category) {

    let li = document.createElement("li");
    if (amount === 1) {
        li.className = "list-group-item list-group-item-danger";
    } else if (amount === 2) {
        li.className = "list-group-item list-group-item-warning";
    } else if (amount === 3) {
        li.className = "list-group-item list-group-item-success";
    }
    li.innerHTML = item;

    let btnMin = document.createElement("button");
    btnMin.className = "btn btn-secondary";
    btnMin.type = "button";
    btnMin.onclick = function () {
        updateItem(id, amount - 1, category);
    };
    btnMin.style.float = "right";

    let iMin = document.createElement("i");
    iMin.className = "fas fa-minus";
    btnMin.appendChild(iMin);

    let btnPlus = document.createElement("button");
    btnPlus.className = "btn btn-secondary";
    btnPlus.type = "button";
    btnPlus.onclick = function () {
        updateItem(id, amount + 1, category);
    };
    btnPlus.style.float = "right";

    let iPlus = document.createElement("i");
    iPlus.className = "fas fa-plus";
    btnPlus.appendChild(iPlus);

    let btnDel = document.createElement("button");
    btnDel.className = "btn btn-secondary";
    btnDel.type = "button";
    btnDel.onclick = function () {
        deleteItem(id, category);
    };
    btnDel.style.float = "right";

    let iDel = document.createElement("i");
    iDel.className = "fas fa-trash-alt";
    btnDel.appendChild(iDel);

    li.appendChild(btnDel);
    li.appendChild(btnPlus);
    li.appendChild(btnMin);

    document.getElementById(category).appendChild(li);
}

function deleteItem(id, category) {
    fetch(urlItems + id,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "DELETE",
        })
        .then(function (res) {
            reloadCategory(category);
            console.log(res)
        })
        .catch(function (res) {
            console.log(res)
        });
}

function updateItem(id, amount, category) {
    if (amount >= 1 && amount <= 3) {
        fetch(urlItems + id,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PATCH",
                body: JSON.stringify({"amount": amount})
            })
            .then(function (res) {
                reloadCategory(category);
                console.log(res)
            })
            .catch(function (res) {
                console.log(res)
            });
    }
}

function validateForm() {
    let item = document.getElementById("item").value;
    document.getElementById("add").disabled = item.length < 2;
}


function addItem() {
    let item = document.getElementById("item").value;
    let amount = document.getElementById("amount").value;
    let category = document.getElementById("category").value;

    let data = {
        "item": item.toString(),
        "category": category,
        "amount": parseInt(amount)
    };
    fetch(urlItems,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(data)
        })
        .then(function (res) {
            if (res.status === 201) {
                document.getElementById("item").value = "";
                let toast = document.getElementById("snackbar");
                toast.innerHTML = item + " hinzuagfüagt";
                toast.className = "show";
                setTimeout(function () {
                    toast.className = toast.className.replace("show", "");
                }, 3000);
            }
            console.log(res)
        })
        .catch(function (res) {
            console.log(res)
        });
    document.getElementById("add").disabled = true;
}

function toggleCategory(category) {
    let node = document.getElementById(category);
    if (node.childElementCount < 1) {
        fetch(urlItems + category)
            .then(res => res.json())
            .then(function (data) {
                for (let i = 0; i < data.length; i++) {
                    createItem(data[i].id, data[i].item, data[i].amount, data[i].category);
                }
            });
    } else {
        while (node.firstChild) {
            node.removeChild(node.firstChild);
        }
    }
}

function reloadCategory(category) {
    let node = document.getElementById(category);
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
    fetch(urlItems + category)
        .then(res => res.json())
        .then(function (data) {
            for (let i = 0; i < data.length; i++) {
                createItem(data[i].id, data[i].item, data[i].amount, data[i].category);
            }
        });
}

function createNote() {
    let notesDiv = document.createElement("div");
    notesDiv.className = "card";
    notesDiv.id = "notesDiv";

    let label = document.createElement("label");
    label.setAttribute("for", "notes");
    label.style.display = "none";

    let txtArea = document.createElement("textarea");
    txtArea.className = "form-control";
    txtArea.id = "notes";
    txtArea.rows = 2;
    txtArea.placeholder = "was sölli s nögschtmol nid vergessa?";
    txtArea.onchange = function () {
        updateNote();
    };
    document.getElementById("notesContainer").appendChild(notesDiv);
    document.getElementById("notesContainer").appendChild(label);
    document.getElementById("notesContainer").appendChild(txtArea);
}

function toggleNote() {
    let node = document.getElementById("notesContainer");
    if (node.childElementCount < 1) {
        createNote();
        fetch(urlNotes)
            .then(res => res.json())
            .then(function (data) {
                document.getElementById("notesDiv").innerHTML = data[0].text;
            });
    } else {
        while (node.firstChild) {
            node.removeChild(node.firstChild);
        }
    }
}

function updateNote() {
    let text = document.getElementById("notes").value;

    fetch(urlNotes,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "PATCH",
            body: JSON.stringify({"text": text.toString()})
        })
        .then(function (res) {
            document.getElementById("notesDiv").innerHTML = text;
            document.getElementById("notes").value = "";
            console.log(res)
        })
        .catch(function (res) {
            console.log(res)
        });
}