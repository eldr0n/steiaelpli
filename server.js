require('dotenv').config();
const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const session = require('express-session');
const { ExpressOIDC } = require('@okta/oidc-middleware');
const { Pool } = require('pg');

const pool = new Pool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT,
    ssl: false
  })

const app = express();
const port = process.env.PORT || 3002;

app.use(session({
    secret: process.env.SECRET,
    resave: true,
    saveUninitialized: false
}));

const oidc = new ExpressOIDC({
    issuer: `${process.env.OKTA_ORG_URL}/oauth2/default`,
    client_id: process.env.OKTA_CLIENT_ID,
    client_secret: process.env.OKTA_CLIENT_SECRET,
    appBaseUrl: process.env.BASE_URL,
    redirect_uri: process.env.REDIRECT_URL,
    scope: 'openid profile',
    routes: {
        loginCallback: {
            afterCallback: '/inventory'
        }
    }
});

app.use(express.static(path.join(__dirname, 'public')));
app.use(oidc.router);
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



app.get('/', (req, res) => {
    res.redirect('/home');
});

app.get("/notes", oidc.ensureAuthenticated(), (req, res, next) => {
    pool.query('SELECT * FROM notes', (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
});

app.patch("/notes", oidc.ensureAuthenticated(), (req, res, next) => {
    pool.query('UPDATE notes SET text = $1 WHERE id = 1', [req.body.text], error => {
        if (error) {
            throw error
        }
        res.status(201).json({ status: 'success', message: 'item updated.' })
    })
});

app.get("/items", oidc.ensureAuthenticated(), (req, res, next) => {
    pool.query('SELECT * FROM inventory ORDER BY item', (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
});

app.get("/items/:cat", oidc.ensureAuthenticated(), (req, res, next) => {
    pool.query('SELECT * FROM inventory WHERE category = $1 ORDER BY item', [req.params.cat], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
});

app.post("/items", oidc.ensureAuthenticated(), (req, res, next) => {
    let { item, category, amount } = req.body;
    pool.query('INSERT INTO inventory (item, category, amount) VALUES ($1, $2, $3)', [item, category, amount], error => {
        if (error) {
            throw error
        }
        res.status(201).json({ status: 'success', message: 'item added.' })
    })
});

app.patch("/items/:id", oidc.ensureAuthenticated(), (req, res, next) => {
    pool.query('UPDATE inventory SET amount = $1 WHERE id = $2', [req.body.amount, req.params.id], error => {
        if (error) {
            throw error
        }
        res.status(201).json({ status: 'success', message: 'item updated.' })
    })
});

app.delete("/items/:id", oidc.ensureAuthenticated(), (req, res, next) => {
    pool.query('DELETE FROM inventory WHERE id = $1', [req.params.id], error => {
        if (error) {
            throw error
        }
        res.status(201).json({ status: 'success', message: 'item deleted.' })
    })
});


app.get('/home', (req, res) => {
    res.sendFile(path.join(__dirname, './public/home.html'));
});

app.get('/inventory', oidc.ensureAuthenticated(), (req, res) =>{
    res.sendFile(path.join(__dirname, './public/inventory.html'));
});


app.get('/logout', function(req, res, next) {
    req.logout(function(err) {
      if (err) { return next(err); }
      res.redirect('/home');
    });
});

app.listen(port, () => console.log(`listening on port ${port}!`));